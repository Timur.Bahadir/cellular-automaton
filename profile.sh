#!/bin/bash

cd build/src/
./cellular-automaton.exe
gprof cellular-automaton.exe gmon.out > profile.txt &&\
python -m gprof2dot -n1.0 -s profile.txt > profile.dot&&\
dot -Tpng profile.dot -o profile.png
cd ..
