#include "ca/states/MenuState.hpp"

#include "ca/states/BriansBrain.hpp"
#include "ca/states/CaveGenerator.hpp"
#include "ca/states/GameOfLife.hpp"
#include "ca/states/WireWorld.hpp"

#include <tibaf/Application.hpp>

#include <string>

namespace ca::states {
MenuState::MenuState(tibaf::Application &app, sf::RenderWindow &window,
                     tgui::Gui &gui)
    : ApplicationState{app, window, gui} {}

void MenuState::enter() {
  tgui::VerticalLayout::Ptr button_layout =
      tgui::VerticalLayout::create({"80%", "60%"});
  gui.add(button_layout);

  tgui::HorizontalLayout::Ptr row0 =
      tgui::HorizontalLayout::create({"100%", "100%"});
  button_layout->add(row0, "row0");

  tgui::HorizontalLayout::Ptr row1 =
      tgui::HorizontalLayout::create({"100%", "100%"});
  button_layout->add(row1, "row1");

  button_layout->setPosition({"10%", "10%"});

  tgui::Button::Ptr game_of_life = tgui::Button::create("Game of Life");
  game_of_life->connect("pressed",
                        [this]() { this->app.change_state<GameOfLife>(); });
  row0->add(game_of_life, "game_of_life");

  tgui::Button::Ptr cave_generator = tgui::Button::create("Cave Generator");
  cave_generator->connect(
      "pressed", [this]() { this->app.change_state<CaveGenerator>(); });
  row0->add(cave_generator, "cave_generator");

  tgui::Button::Ptr brians_brain = tgui::Button::create("Brians Brain");
  brians_brain->connect("pressed",
                        [this]() { this->app.change_state<BriansBrain>(); });
  row0->add(brians_brain, "brians_brain");

  tgui::Button::Ptr wire_world = tgui::Button::create("Wire World");
  wire_world->connect("pressed",
                      [this]() { this->app.change_state<WireWorld>(); });
  row1->add(wire_world, "wire_world");

  tgui::Button::Ptr test2 = tgui::Button::create("Test");
  row1->add(test2, "test2");

  tgui::Button::Ptr test3 = tgui::Button::create("Test");
  row1->add(test3, "test3");

  tgui::Button::Ptr quit = tgui::Button::create("Quit");
  gui.add(quit, "Quit");
  quit->connect("pressed", [this]() { this->app.quit(); });
  quit->setSize({"30%", "10%"});
  quit->setPosition(
      {"(&.width / 2) - (width / 2)", "(&.height - height) - 10"});
}

void MenuState::handle_input(sf::Event const &event) {}
void MenuState::update(float delta) {}
void MenuState::draw() {}

void MenuState::exit() { gui.removeAllWidgets(); }

} // namespace ca::states
