#include "ca/states/WireWorld.hpp"

#include <tibaf/Application.hpp>

namespace ca::states {
WireWorld::WireWorld(tibaf::Application &app, sf::RenderWindow &window,
                     tgui::Gui &gui)
    : CAApplicationState{app, window, gui, false, 1.0f},
      field_init{[](std::size_t x, std::size_t y) { return States::Empty; }},
      rules_callback{[](States me, WWCA::Neighbors const &neighbors,
                        std::uint8_t my_index) -> States {
        std::uint8_t const head_neighbors =
            neighbors[WWCA::enum_to_index(States::Head)];
        switch (me) {
        case States::Empty:
          return States::Empty;
        case States::Head:
          return States::Trail;
        case States::Trail:
          return States::Conductor;
        case States::Conductor:
          if (head_neighbors == 1 || head_neighbors == 2) {
            return States::Head;
          } else {
            return States::Conductor;
          }
        default:
          return States::Empty;
        }
      }},
      enum_to_color{[](States state) {
        switch (state) {
        case States::Empty:
          return sf::Color{17, 47, 65};
        case States::Head:
          return sf::Color{237, 85, 59};
        case States::Trail:
          return sf::Color{8, 148, 161};
        case States::Conductor:
          return sf::Color{242, 177, 52};
        default:
          return sf::Color::Magenta;
        }
      }},
      cellular_automaton{sf::Vector2u{100, 100}, window.getSize(), field_init,
                         rules_callback, enum_to_color} {}

void WireWorld::handle_input(sf::Event const &event) {
  if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)) {
    sf::Vector2i mouse_position{sf::Mouse::getPosition(window)};
    cellular_automaton.set_at(mouse_position.x, mouse_position.y,
                              States::Conductor);
  }
  if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Right)) {
    sf::Vector2i mouse_position{sf::Mouse::getPosition(window)};
    cellular_automaton.set_at(mouse_position.x, mouse_position.y,
                              States::Empty);
  }
  if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Middle)) {
    sf::Vector2i mouse_position{sf::Mouse::getPosition(window)};
    cellular_automaton.set_at(mouse_position.x, mouse_position.y, States::Head);
  }
}

void WireWorld::reset() { cellular_automaton.reset(); }

void WireWorld::tick() { cellular_automaton.do_tick(); }

void WireWorld::draw() { cellular_automaton.draw(window); }

} // namespace ca::states
