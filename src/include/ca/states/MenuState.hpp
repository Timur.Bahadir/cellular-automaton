#pragma once

#include <tibaf/Application.hpp>
#include <tibaf/ApplicationState.hpp>

namespace ca::states {

class MenuState : public tibaf::ApplicationState {
public:
  MenuState(tibaf::Application &app, sf::RenderWindow &window, tgui::Gui &gui);

  void enter() override;

  void handle_input(sf::Event const &event) override;
  void update(float delta) override;
  void draw() override;

  void exit() override;
};

} // namespace ca::states
