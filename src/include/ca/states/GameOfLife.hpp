#pragma once

#include "ca/CellularAutomaton.hpp"
#include "ca/states/CAApplicationState.hpp"

#include <tibaf/Application.hpp>

namespace ca::states {

class GameOfLife : public CAApplicationState {
public:
  GameOfLife(tibaf::Application &app, sf::RenderWindow &window, tgui::Gui &gui);

  void handle_input(sf::Event const &event) override;
  void draw() override;

protected:
  virtual void reset() override;
  virtual void tick() override;

private:
  enum class States : std::uint8_t { Dead, Alive };
  using GoLCA = ca::CellularAutomaton<States, States::Dead, 2, EdgeMode::Wrap>;

  GoLCA::FieldInit field_init;
  GoLCA::TickCallback rules_callback;
  GoLCA::EnumToColor enum_to_color;

  GoLCA cellular_automaton;
};

} // namespace ca::states
