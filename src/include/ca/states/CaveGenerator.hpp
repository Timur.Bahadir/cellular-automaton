#pragma once

#include "ca/CellularAutomaton.hpp"
#include "ca/states/CAApplicationState.hpp"

#include <random>

#include <tibaf/Application.hpp>

namespace ca::states {

class CaveGenerator : public CAApplicationState {
public:
  CaveGenerator(tibaf::Application &app, sf::RenderWindow &window,
                tgui::Gui &gui);

  void enter() override;

  void handle_input(sf::Event const &event) override;
  void draw() override;

protected:
  virtual void reset() override;
  virtual void tick() override;

private:
  enum class States : std::uint8_t { Empty, Solid };
  using CaveGeneratorCA =
      ca::CellularAutomaton<States, States::Solid, 2, EdgeMode::Skip>;

  std::random_device rd;
  std::mt19937 mt;
  std::uniform_real_distribution<float> dist;

  int initial_empty_percent;

  CaveGeneratorCA::FieldInit field_init;
  CaveGeneratorCA::TickCallback rules_callback;
  CaveGeneratorCA::EnumToColor enum_to_color;

  CaveGeneratorCA cellular_automaton;
};

} // namespace ca::states
