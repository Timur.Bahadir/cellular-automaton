#pragma once

#include "ca/CellularAutomaton.hpp"

#include <tibaf/Application.hpp>
#include <tibaf/ApplicationState.hpp>

namespace ca::states {

class CAApplicationState : public tibaf::ApplicationState {
public:
  CAApplicationState(tibaf::Application &app, sf::RenderWindow &window,
                     tgui::Gui &gui, bool sr, float tps);

  void enter() override;

  void handle_input(sf::Event const &event) = 0;
  void update(float delta) override;
  void draw() = 0;

  void exit() override;

protected:
  virtual void reset() = 0;
  virtual void tick() = 0;

  tgui::Button::Ptr toggle_simulation;

  bool simulation_running;
  float ticks_per_second;
  float tick_delta;
};

} // namespace ca::states
