#pragma once

#include "ca/CellularAutomaton.hpp"
#include "ca/states/CAApplicationState.hpp"

#include <tibaf/Application.hpp>

namespace ca::states {

class WireWorld : public CAApplicationState {
public:
  WireWorld(tibaf::Application &app, sf::RenderWindow &window, tgui::Gui &gui);

  void handle_input(sf::Event const &event) override;
  void draw() override;

protected:
  virtual void reset() override;
  virtual void tick() override;

private:
  enum class States : std::uint8_t { Empty, Head, Trail, Conductor };
  using WWCA = ca::CellularAutomaton<States, States::Empty, 4, EdgeMode::Skip>;

  WWCA::FieldInit field_init;
  WWCA::TickCallback rules_callback;
  WWCA::EnumToColor enum_to_color;

  WWCA cellular_automaton;
};

} // namespace ca::states
