#pragma once

#include "ca/CellularAutomaton.hpp"
#include "ca/states/CAApplicationState.hpp"

#include <tibaf/Application.hpp>

namespace ca::states {

class BriansBrain : public CAApplicationState {
public:
  BriansBrain(tibaf::Application &app, sf::RenderWindow &window, tgui::Gui &gui);

  void handle_input(sf::Event const &event) override;
  void draw() override;

protected:
  virtual void reset() override;
  virtual void tick() override;

private:
  enum class States : std::uint8_t { Dead, Dying, Alive };
  using BBCA = ca::CellularAutomaton<States, States::Dead, 3, EdgeMode::Wrap>;

  BBCA::FieldInit field_init;
  BBCA::TickCallback rules_callback;
  BBCA::EnumToColor enum_to_color;

  BBCA cellular_automaton;
};

} // namespace ca::states
