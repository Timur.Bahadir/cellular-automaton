#pragma once

#include <SFML/Graphics.hpp>

#include <array>
#include <cstdint>
#include <functional>
#include <typeindex>
#include <typeinfo>
#include <unordered_map>
#include <vector>

namespace ca {

enum class EdgeMode : uint8_t { Skip, Handle, Wrap };

template <typename Enum, Enum Initial, std::uint8_t Count,
          EdgeMode EMode = EdgeMode::Skip>
class CellularAutomaton {
public:
  using Neighbors = std::array<std::uint8_t, Count>;
  using FieldInit = std::function<Enum(std::size_t, std::size_t)>;
  using TickCallback =
      std::function<Enum(Enum, Neighbors const &, std::uint8_t)>;
  using EnumToColor = std::function<sf::Color(Enum)>;

  CellularAutomaton(sf::Vector2u board_size, sf::Vector2u window_size,
                    FieldInit const &fi, TickCallback const &tc,
                    EnumToColor const &etc)
      : field{board_size.x * board_size.y, Initial}, board_size{board_size},
        window_size{window_size}, field_init{fi}, tick_callback{tc},
        enum_to_color{etc}, image{}, texture{}, sprite{} {

    image.create(board_size.x, board_size.y);
    texture.create(board_size.x, board_size.y);
    reset();
    sprite.setTexture(texture);
    sprite.setScale(window_size.x / static_cast<float>(board_size.x),
                    window_size.y / static_cast<float>(board_size.y));
  }
  ~CellularAutomaton() {}

  void do_tick() {
    std::vector<Enum> op_field{field};

    std::size_t x_start, x_end;
    std::size_t y_start, y_end;

    if (EMode == EdgeMode::Skip) {
      x_start = 1;
      x_end = board_size.x - 1;
      y_start = 1;
      y_end = board_size.y - 1;
    } else if (EMode == EdgeMode::Handle || EMode == EdgeMode::Wrap) {
      x_start = 0;
      x_end = board_size.x;
      y_start = 0;
      y_end = board_size.y;
    }

    for (std::size_t y = y_start; y < y_end; ++y) {
      for (std::size_t x = x_start; x < x_end; ++x) {
        Enum const me = op_field[field_index(x, y)];
        Neighbors neighbors{0u};

        for (std::int8_t oy = -1; oy < 2; ++oy) {
          for (std::int8_t ox = -1; ox < 2; ++ox) {
            if (oy == 0 && ox == 0) {
              continue;
            }

            std::size_t mode_x, mode_y;
            if (EMode == EdgeMode::Skip) {
              mode_x = x + ox;
              mode_y = y + oy;
            } else if (EMode == EdgeMode::Handle) {
              if (x == 0 && ox < 0) {
                continue;
              } else if (x == board_size.x - 1 && ox > 0) {
                continue;
              } else {
                mode_x = x + ox;
              }

              if (y == 0 && oy < 0) {
                continue;
              } else if (y == board_size.y - 1 && oy > 0) {
                continue;
              } else {
                mode_y = y + oy;
              }
            } else if (EMode == EdgeMode::Wrap) {
              if (x == 0 && ox < 0) {
                mode_x = board_size.x - 1;
              } else if (x == board_size.x - 1 && ox > 0) {
                mode_x = 0;
              } else {
                mode_x = x + ox;
              }

              if (y == 0 && oy < 0) {
                mode_y = board_size.y - 1;
              } else if (y == board_size.y - 1 && oy > 0) {
                mode_y = 0;
              } else {
                mode_y = y + oy;
              }
            }

            Enum const n = op_field[field_index(mode_x, mode_y)];
            ++neighbors.at(enum_to_index(n));
          }
        }

        field[field_index(x, y)] =
            tick_callback(me, neighbors, enum_to_index(me));
        image.setPixel(x, y, enum_to_color(field[field_index(x, y)]));
      }
    }

    texture.update(image);
  }

  void draw(sf::RenderWindow &window) { window.draw(sprite); }

  void reset() {
    for (std::size_t y = 0; y < board_size.y; ++y) {
      for (std::size_t x = 0; x < board_size.x; ++x) {
        field[field_index(x, y)] = field_init(x, y);
        image.setPixel(x, y, enum_to_color(field[field_index(x, y)]));
      }
    }
    texture.loadFromImage(image);
  }

  void set_at(std::size_t x, std::size_t y, Enum e) {
    sf::Vector2f fbs{board_size}, fws{window_size};
    std::size_t field_x{static_cast<std::size_t>((fbs.x / fws.x) * x)};
    std::size_t field_y{static_cast<std::size_t>((fbs.y / fws.y) * y)};

    if (field_x < 0 || field_x > board_size.x || field_y < 0 ||
        field_y > board_size.y) {
      return;
    }

    field[field_index(field_x, field_y)] = e;
    image.setPixel(field_x, field_y, enum_to_color(e));
    texture.update(image);
  }

  static std::uint8_t enum_to_index(Enum e) {
    return static_cast<std::uint8_t>(e);
  }

private:
  std::size_t field_index(std::size_t x, std::size_t y) {
    return y * board_size.x + x;
  }

  std::vector<Enum> field;
  sf::Vector2u board_size;
  FieldInit field_init;
  TickCallback tick_callback;

  EnumToColor enum_to_color;

  sf::Vector2u window_size;
  sf::Image image;
  sf::Texture texture;
  sf::Sprite sprite;
};
} // namespace ca
