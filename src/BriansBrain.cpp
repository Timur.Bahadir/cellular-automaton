#include "ca/states/BriansBrain.hpp"

#include <tibaf/Application.hpp>

namespace ca::states {
BriansBrain::BriansBrain(tibaf::Application &app, sf::RenderWindow &window,
                         tgui::Gui &gui)
    : CAApplicationState{app, window, gui, false, 1.0f},
      field_init{[](std::size_t x, std::size_t y) { return States::Dead; }},
      rules_callback{[](States me, BBCA::Neighbors const &neighbors,
                        std::uint8_t my_index) -> States {
        std::uint8_t const alive_neighbors =
            neighbors[BBCA::enum_to_index(States::Alive)];
        switch (me) {
        case States::Dead:
          if (alive_neighbors == 2) {
            return States::Alive;
          } else {
            return States::Dead;
          }
        case States::Alive:
          return States::Dying;
        case States::Dying:
          return States::Dead;
        default:
          return States::Dead;
        }
      }},
      enum_to_color{[](States state) {
        switch (state) {
        case States::Dead:
          return sf::Color{17, 47, 65};
        case States::Dying:
          return sf::Color{71, 171, 108};
        case States::Alive:
          return sf::Color{237, 85, 59};
        default:
          return sf::Color::Magenta;
        }
      }},
      cellular_automaton{sf::Vector2u{100, 100}, window.getSize(), field_init,
                         rules_callback, enum_to_color} {}

void BriansBrain::handle_input(sf::Event const &event) {
  if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)) {
    sf::Vector2i mouse_position{sf::Mouse::getPosition(window)};
    cellular_automaton.set_at(mouse_position.x, mouse_position.y,
                              States::Alive);
  }
  if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Right)) {
    sf::Vector2i mouse_position{sf::Mouse::getPosition(window)};
    cellular_automaton.set_at(mouse_position.x, mouse_position.y, States::Dead);
  }
}

void BriansBrain::reset() { cellular_automaton.reset(); }

void BriansBrain::tick() { cellular_automaton.do_tick(); }

void BriansBrain::draw() { cellular_automaton.draw(window); }

} // namespace ca::states
