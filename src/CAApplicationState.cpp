#include "ca/states/CAApplicationState.hpp"

#include <cstdlib>

#include "ca/states/MenuState.hpp"

namespace ca::states {
CAApplicationState::CAApplicationState(tibaf::Application &app,
                                       sf::RenderWindow &window, tgui::Gui &gui,
                                       bool sr, float tps)
    : ApplicationState{app, window, gui}, simulation_running{sr},
      ticks_per_second{tps}, tick_delta{0.0f} {}

void CAApplicationState::enter() {
  tgui::Button::Ptr back = tgui::Button::create("Menu");
  back->connect("pressed", [this]() { this->app.change_state<MenuState>(); });
  gui.add(back, "Back");
  back->setSize(100, 20);

  tgui::Button::Ptr reset_ca = tgui::Button::create("Reset");
  reset_ca->connect("pressed", [this]() { this->reset(); });
  gui.add(reset_ca, "Reset");
  reset_ca->setSize({"Back.width", "Back.height"});
  reset_ca->setPosition({"Back.left", "Back.bottom + 10"});

  tgui::Button::Ptr tick = tgui::Button::create("Tick");
  tick->connect("pressed", [this]() { this->tick(); });
  gui.add(tick, "Tick");
  tick->setSize({"Back.width", "Back.height"});
  tick->setPosition({"Back.left", "Reset.bottom + 10"});

  toggle_simulation = tgui::Button::create("Start");
  toggle_simulation->connect("pressed", [this]() {
    this->simulation_running = !this->simulation_running;
    if (this->simulation_running) {
      this->toggle_simulation->setText("Stop");
    } else {
      this->toggle_simulation->setText("Start");
    }
  });
  gui.add(toggle_simulation, "ToggleSimulation");
  toggle_simulation->setSize({"Back.width", "Back.height"});
  toggle_simulation->setPosition({"Back.left", "Tick.bottom + 10"});

  tgui::Slider::Ptr simulation_speed = tgui::Slider::create(0.1f, 60.0f);
  simulation_speed->setValue(ticks_per_second);
  simulation_speed->connect("ValueChanged", [this](float new_value) {
    this->ticks_per_second = new_value;
  });
  gui.add(simulation_speed, "SimulationSpeed");
  simulation_speed->setSize({"Back.width", "Back.height"});
  simulation_speed->setPosition({"Back.left", "ToggleSimulation.bottom + 10"});
}

void CAApplicationState::update(float delta) {
  tick_delta += delta;
  if (simulation_running) {
    if (tick_delta >= 1.0f / ticks_per_second) {
      tick();
      tick_delta = 0.0f;
    }
  }
}

void CAApplicationState::exit() { gui.removeAllWidgets(); }

} // namespace ca::states
