#include "ca/states/CaveGenerator.hpp"

#include <tibaf/Application.hpp>

#include <cstdlib>

namespace ca::states {
CaveGenerator::CaveGenerator(tibaf::Application &app, sf::RenderWindow &window,
                             tgui::Gui &gui)
    : CAApplicationState{app, window, gui, false, 1.0f}, rd{}, mt{rd()},
      dist{0.0f, 1.0f}, initial_empty_percent{55},
      field_init{[this](std::size_t x, std::size_t y) {
        if (x == 0 || y == 0 || x == 100 - 1 || y == 100 - 1) {
          return States::Solid;
        }
        float const rand = std::rand() % 100;
        if (rand > this->initial_empty_percent) {
          return States::Solid;
        } else {
          return States::Empty;
        }
      }},
      rules_callback{[](States me, CaveGeneratorCA::Neighbors const &neighbors,
                        std::uint8_t my_index) -> States {
        std::uint8_t const alive_neighbors =
            neighbors[CaveGeneratorCA::enum_to_index(States::Solid)];
        if (me == States::Solid) {
          if (alive_neighbors < 4) {
            return States::Empty;
          } else {
            return States::Solid;
          }
        } else {
          if (alive_neighbors >= 5) {
            return States::Solid;
          } else {
            return States::Empty;
          }
        }
      }},
      enum_to_color{[](States state) {
        switch (state) {
        case States::Empty:
          return sf::Color{17, 47, 65};
        case States::Solid:
          return sf::Color{237, 85, 59};
        default:
          return sf::Color::Magenta;
        }
      }},
      cellular_automaton{sf::Vector2u{100, 100}, window.getSize(), field_init,
                         rules_callback, enum_to_color} {}

void CaveGenerator::enter() {
  CAApplicationState::enter();

  tgui::Slider::Ptr initial_empty = tgui::Slider::create(0.0f, 100.0f);
  initial_empty->setValue(initial_empty_percent);
  initial_empty->connect("ValueChanged", [this](float new_value) {
    this->initial_empty_percent = new_value;
  });
  gui.add(initial_empty, "InitialEmpty");
  initial_empty->setSize({"Back.width", "Back.height"});
  initial_empty->setPosition({"Back.left", "SimulationSpeed.bottom + 10"});
}

void CaveGenerator::handle_input(sf::Event const &event) {
  if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)) {
    sf::Vector2i mouse_position{sf::Mouse::getPosition(window)};
    cellular_automaton.set_at(mouse_position.x, mouse_position.y,
                              States::Solid);
  }
  if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Right)) {
    sf::Vector2i mouse_position{sf::Mouse::getPosition(window)};
    cellular_automaton.set_at(mouse_position.x, mouse_position.y,
                              States::Empty);
  }
}

void CaveGenerator::reset() { cellular_automaton.reset(); }

void CaveGenerator::tick() { cellular_automaton.do_tick(); }

void CaveGenerator::draw() { cellular_automaton.draw(window); }

} // namespace ca::states
