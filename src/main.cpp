#include <tibaf/Application.hpp>

#include <SFML/Graphics.hpp>

#include "ca/states/MenuState.hpp"

int main() {
  tibaf::Application app{sf::VideoMode{600, 600}, "Cellular Automaton"};
  app.change_state<ca::states::MenuState>();
  app.main_loop();
  return 0;
}