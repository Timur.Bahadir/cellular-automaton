# Cellular Automaton

A small framework/sandbox for the implementation of
different cellular automatons.

## Build

### Requirements:

* C++ Compiler
* CMake
* SFML 2.5.1
* TGUI (Texus GUI)

Installing SFML

```bash
git clone https://github.com/SFML/SFML.git
cd SFML
git checkout 2.5.1
mkdir build
cmake ..
cmake --build . --target install -- -j 4
```

Installing TGUI

```bash
git clone https://github.com/texus/TGUI.git
cd TGUI
git checkout v0.8.4
mkdir build
cd build
cmake .. -DSFML_DIR="<.../path-to/SFML/>"
cmake --build . --target install -- -j 4
```

### Build Application

```bash
mkdir build
cd build
cmake ..
cmake --build . -- -j 4
```

## Features

The currently implemented cellular automaton are:

* Game of Life
* Generation of cave like structures
* Brians Brain
* Wire World

## Implementing new cellular automaton

The easiest way to add a new cellular automaton is to copy and paste
a ApplicationState containing one of the already existing ones, e.g.:
GameOfLife.hpp and GameOfLife.cpp. Then follow these steps.

* Rename copy pasted files.
* Rename the class inheriting ApplicationState
* Add the new ApplicationState to the ApplicationStates enum
* Change the States enum
* Rename the using definition
* Implement the InitField, TickCallback and EnumToColor callbacks
* Add the new ApplicationStates enum value to the Application::change_state switch
* Add a button to MenuState for the new ApplicationState

## Media

A video showing the development of the application and examples of the cellular
automatons running, can be found [here](https://www.youtube.com/watch?v=YzT-nDhnyLs).
